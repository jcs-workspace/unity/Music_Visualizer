# Unity - Music Visualizer #

Unity music visualizer implementation. <br/><br/>


## How to use? ##
Under Unity's data path create folder `mv_dir` and drop all the `.ogg` 
files in there and it should load all the `.ogg` files under that directory. 

Currently the music player will loop all the audio files forever, 
there are three buttons at the bottom of the scene. `Play` or `Pause`, 
`Skip` and `Replay`.


## Demo ##
<a href="https://www.youtube.com/watch?v=BkKbOezO3Vs&t=158s
" target="_blank"><img src="http://i3.ytimg.com/vi/BkKbOezO3Vs/maxresdefault.jpg" 
alt="IMAGE ALT TEXT HERE" width="480" height="270" border="10" /></a>
