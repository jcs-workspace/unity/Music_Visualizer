/**
 * $File: MV_MusicPlayer.cs $
 * $Date: 2018-09-25 04:28:49 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information 
 *	                 Copyright © 2018 by Shen, Jen-Chieh $
 */
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Globalization;
using UnityEngine;
using JCSUnity;


/// <summary>
/// Play the music in loop
/// </summary>
[RequireComponent(typeof(JCS_SoundPlayer))]
public class MV_MusicPlayer 
    : MonoBehaviour 
{

    /*******************************************/
    /*            Public Variables             */
    /*******************************************/

    // Singleton.
    public static MV_MusicPlayer instance = null;

    /*******************************************/
    /*           Private Variables             */
    /*******************************************/

    private JCS_SoundPlayer mSoundPlayer = null;


    [Header("** Check Variables (MV_MusicPlayer) **")]

    [Tooltip("")]
    [SerializeField]
    private List<AudioClip> mAudioClips = null;

    [SerializeField]
    private List<string> mClipNames = null;

    [Tooltip("")]
    [SerializeField]
    private int mCurrentPlayIndex = -1;

    [Tooltip("Current music play is pausing?")]
    private bool mIsPausing = false;


    /*******************************************/
    /*           Protected Variables           */
    /*******************************************/

    /*******************************************/
    /*             setter / getter             */
    /*******************************************/
    public bool IsPausing { get { return this.mIsPausing; } }
    public List<AudioClip> AudioClips { get { return this.mAudioClips; } }

    /*******************************************/
    /*            Unity's function             */
    /*******************************************/
    private void Awake()
    {
        instance = this;

        mSoundPlayer = this.GetComponent<JCS_SoundPlayer>();
    }

    private void Start()
    {
        MV_GameManager gm = MV_GameManager.instance;

        string musicRootPath = gm.MUSIC_ROOT_PATH;
        string musicExt = gm.MUSIC_EXTENSION;

        // Only search the ogg file.
        string[] allFiles = Directory.GetFiles(musicRootPath, musicExt, SearchOption.AllDirectories);

        for (int index = 0;
            index < allFiles.Length;
            ++index)
        {
            string path = allFiles[index];

            StartCoroutine(LoadAudio(path));
        }
    }

    private void Update()
    {
        // Play music loop
        AudioSource tmpAS = mSoundPlayer.GetAudioSource();

        if (tmpAS.isPlaying)
            return;

        if (mIsPausing)
            return;

        if (mAudioClips.Count == 0)
            return;

        SkipMusic();
    }

    /*******************************************/
    /*              Self-Define                */
    /*******************************************/
    //----------------------
    // Public Functions

    /// <summary>
    /// Play or pause the music player.
    /// </summary>
    public void PlayOrPause()
    {
        AudioSource tmpAS = mSoundPlayer.GetAudioSource();

        if (mIsPausing)
        {
            tmpAS.Play();
        }
        else
        {
            tmpAS.Pause();
        }

        // Toggle it.
        mIsPausing = !mIsPausing;
    }

    /// <summary>
    /// Replay it.
    /// </summary>
    public void ReplayMusic()
    {
        AudioSource tmpAS = mSoundPlayer.GetAudioSource();

        if (tmpAS.clip == null)
            return;

        tmpAS.time = 0.0f;

        tmpAS.Play();

        {
            // No longer pausing.
            mIsPausing = false;

            // Update UI.
            MV_UIManager.instance.BTN_PLAY_PAUSE.SetButtonByIsPausingFlag();
        }
    }

    /// <summary>
    /// Skip this music and play next music.
    /// </summary>
    public void SkipMusic()
    {
        ++mCurrentPlayIndex;

        // Make sure it loop.
        if (mCurrentPlayIndex >= mAudioClips.Count)
            mCurrentPlayIndex = 0;

        PlayOneMusic();
        
        {
            // No longer pausing.
            mIsPausing = false;

            // Update UI.
            MV_UIManager.instance.BTN_PLAY_PAUSE.SetButtonByIsPausingFlag();
        }
    }

    //----------------------
    // Protected Functions

    //----------------------
    // Private Functions

    /// <summary>
    /// Load the music from path in runtime.
    /// </summary>
    /// <param name="ac"> A container for audio data. </param>
    /// <param name="fullFilePath"> Filpath to the target audio file. </param>
    /// <param name="callback"> Callback after the audio is loaded. </param>
    /// <returns> Coroutine status. </returns>
    public IEnumerator LoadAudio(
        string fullFilePath)
    {
        string formatFullFilePath = string.Format("file://{0}", fullFilePath);

        WWW request = new WWW(formatFullFilePath);
        yield return request;

        AudioClip ac = request.GetAudioClip(false, false);

        mAudioClips.Add(ac);

        mClipNames.Add(Path.GetFileName(formatFullFilePath));
    }

    /// <summary>
    /// Play current index music.
    /// </summary>
    private void PlayOneMusic()
    {
        AudioSource tmpAS = mSoundPlayer.GetAudioSource();

        if (mAudioClips.Count == 0)
            return;

        AudioClip ac = mAudioClips[mCurrentPlayIndex];

        tmpAS.clip = ac;
        MV_UIManager.instance.TXT_SONG_NAME.text = mClipNames[mCurrentPlayIndex];

        tmpAS.Play();
    }
}
