/**
 * $File: MV_Button.cs $
 * $Date: 2018-09-25 16:33:44 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information 
 *	                 Copyright © 2018 by Shen, Jen-Chieh $
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JCSUnity;


/// <summary>
/// MV's 3D Button base class.
/// </summary>
[RequireComponent(typeof(JCS_SlideEffect))]
public abstract class MV_Button 
    : MonoBehaviour 
{

    /*******************************************/
    /*            Public Variables             */
    /*******************************************/

    /*******************************************/
    /*           Private Variables             */
    /*******************************************/

    /*******************************************/
    /*           Protected Variables           */
    /*******************************************/

    [Header("** Check Variables (MV_Button) **")]

    [Tooltip("Slide the transform.")]
    [SerializeField]
    protected JCS_SlideEffect mSlideEffect = null;


    [Header("** Initialize Variables (MV_Button) **")]

    [Tooltip("")]
    [SerializeField]
    protected MV_ButtonText mButtonText = null;


    [Header("** Runtime Variables (MV_Button) **")]

    [Tooltip("GUI representation in 3D, just a cube as default.")]
    [SerializeField]
    protected Transform m3DGUI = null;

    [Tooltip("What's the original button color.")]
    [SerializeField]
    protected Color mButtonColor = Color.white;

    /*******************************************/
    /*             setter / getter             */
    /*******************************************/

    /*******************************************/
    /*            Unity's function             */
    /*******************************************/
    protected virtual void Awake()
    {
        this.mSlideEffect = this.GetComponent<JCS_SlideEffect>();
    }

    protected virtual void Start()
    {
        // Wait to override..
    }

    protected virtual void Update()
    {
        if (mButtonText.PCA == null)
            return;

        if (m3DGUI != null)
            mButtonText.PCA.CastToScreen(this.m3DGUI.position);
        else
            mButtonText.PCA.CastToScreen(this.transform.position);


        UpdateButtonColor();
    }

    protected virtual void OnMouseEnter()
    {
        Active();
    }

    protected virtual void OnMouseExit()
    {
        Deactive();
    }

    protected virtual void OnMouseDown()
    {
        OnClick();
    }

    /*******************************************/
    /*              Self-Define                */
    /*******************************************/
    //----------------------
    // Public Functions

    // <summary>
    /// Set the color of this button. (own/original)
    /// </summary>
    public void UpdateButtonColor()
    {
        // own color.
        UpdateButtonColor(mButtonColor);
    }

    /// <summary>
    /// Set the color of this button.
    /// </summary>
    /// <param name="newColor"> new color you want to apply. </param>
    public void UpdateButtonColor(Color newColor)
    {
        MV_MusicPlayer mp = MV_MusicPlayer.instance;

        Color targetColor = newColor;

        if (mp.AudioClips.Count == 0)
            targetColor = Color.gray;

        Renderer tmpRenderer = m3DGUI.GetComponent<Renderer>();

        tmpRenderer.material.color = targetColor;

        tmpRenderer.material.SetColor("_EmissionColor", targetColor);
    }

    //----------------------
    // Protected Functions

    /// <summary>
    /// On click this 3d button.
    /// </summary>
    protected abstract void OnClick();

    //----------------------
    // Private Functions

    /// <summary>
    /// Active the current button. Mainly mean this button 
    /// is selected or being focused.
    /// </summary>
    private void Active()
    {
        mSlideEffect.Active();

        mButtonText.FadeObject.FadeIn();
    }

    /// <summary>
    /// Deactive the current button, the button is no longer 
    /// being selected or being focused.
    /// </summary>
    private void Deactive()
    {
        mSlideEffect.Deactive();

        mButtonText.FadeObject.FadeOut();
    }

}
