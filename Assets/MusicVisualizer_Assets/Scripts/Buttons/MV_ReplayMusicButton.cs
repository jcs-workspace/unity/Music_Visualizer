/**
 * $File: MV_ReplayMusicButton.cs $
 * $Date: 2018-09-25 20:32:48 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information 
 *	                 Copyright © 2018 by Shen, Jen-Chieh $
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JCSUnity;


/// <summary>
/// Replay the current music.
/// </summary>
public class MV_ReplayMusicButton 
    : MV_Button
{

    /*******************************************/
    /*            Public Variables             */
    /*******************************************/

    /*******************************************/
    /*           Private Variables             */
    /*******************************************/

    /*******************************************/
    /*           Protected Variables           */
    /*******************************************/

    /*******************************************/
    /*             setter / getter             */
    /*******************************************/

    /*******************************************/
    /*            Unity's function             */
    /*******************************************/

    /*******************************************/
    /*              Self-Define                */
    /*******************************************/
    //----------------------
    // Public Functions

    //----------------------
    // Protected Functions

    /// <summary>
    /// On click this 3d button.
    /// </summary>
    protected override void OnClick()
    {
        MV_MusicPlayer.instance.ReplayMusic();
    }

    //----------------------
    // Private Functions

}
