/**
 * $File: MV_SkipMusicButton.cs $
 * $Date: 2018-09-25 16:59:43 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information 
 *	                 Copyright © 2018 by Shen, Jen-Chieh $
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using JCSUnity;


/// <summary>
/// Skip the music button.
/// </summary>
public class MV_SkipMusicButton
    : MV_Button
{

    /*******************************************/
    /*            Public Variables             */
    /*******************************************/

    /*******************************************/
    /*           Private Variables             */
    /*******************************************/

    /*******************************************/
    /*           Protected Variables           */
    /*******************************************/

    /*******************************************/
    /*             setter / getter             */
    /*******************************************/

    /*******************************************/
    /*            Unity's function             */
    /*******************************************/

    /*******************************************/
    /*              Self-Define                */
    /*******************************************/
    //----------------------
    // Public Functions

    //----------------------
    // Protected Functions

    /// <summary>
    /// On click this 3d button.
    /// </summary>
    protected override void OnClick()
    {
        MV_MusicPlayer.instance.SkipMusic();
    }

    //----------------------
    // Private Functions

}
