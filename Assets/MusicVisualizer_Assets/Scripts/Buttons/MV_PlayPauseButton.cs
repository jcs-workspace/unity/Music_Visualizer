/**
 * $File: MV_PlayPauseButton.cs $
 * $Date: 2018-09-25 19:45:34 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information 
 *	                 Copyright © 2018 by Shen, Jen-Chieh $
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JCSUnity;


/// <summary>
/// Play and Pause music player toggle.
/// </summary>
public class MV_PlayPauseButton 
    : MV_Button 
{

    /*******************************************/
    /*            Public Variables             */
    /*******************************************/

    /*******************************************/
    /*           Private Variables             */
    /*******************************************/

    [Header("** Check Variables (MV_PlayPauseButton) **")]

    [Tooltip("Since the music start at beginning so we just have the " +
        "default value as color red.")]
    [SerializeField]
    private Color mTargetColor = Color.red;

    [Tooltip("Current asymtotic color.")]
    [SerializeField]
    private Color mCurrentColor = Color.red;


    [Header("** Runtime Variables (MV_PlayPauseButton) **")]

    [Tooltip("How fast the color changes?")]
    [SerializeField]
    [Range(0.01f, 10.0f)]
    private float mFriction = 0.2f;

    /*******************************************/
    /*           Protected Variables           */
    /*******************************************/

    /*******************************************/
    /*             setter / getter             */
    /*******************************************/

    /*******************************************/
    /*            Unity's function             */
    /*******************************************/
    protected override void Start()
    {
        base.Start();

        SetButtonByIsPausingFlag();
    }

    protected override void Update()
    {
        base.Update();

        mCurrentColor += (mTargetColor - mCurrentColor) / mFriction * Time.deltaTime;

        UpdateButtonColor(mCurrentColor);
    }

    /*******************************************/
    /*              Self-Define                */
    /*******************************************/
    //----------------------
    // Public Functions

    /// <summary>
    /// Play or pause the music player.
    /// </summary>
    public void PlayOrPause()
    {
        MV_MusicPlayer mp = MV_MusicPlayer.instance;

        mp.PlayOrPause();

        SetButtonByIsPausingFlag();
    }

    /// <summary>
    /// Do the button play and pause algorithm here..
    /// </summary>
    public void SetButtonByIsPausingFlag()
    {
        MV_MusicPlayer mp = MV_MusicPlayer.instance;

        // No audio? then do nothing.
        if (mp.AudioClips.Count == 0)
            return;

        // Next time to hit this button, is going to play!
        if (mp.IsPausing)
        {
            mButtonText.TextComp.text = "Play";

            mTargetColor = Color.green;
        }
        // Next time to hit this button, is going to pause!
        else
        {
            mButtonText.TextComp.text = "Pause";

            mTargetColor = Color.red;
        }
    }

    //----------------------
    // Protected Functions

    /// <summary>
    /// On click this 3d button.
    /// </summary>
    protected override void OnClick()
    {
        PlayOrPause();
    }

    //----------------------
    // Private Functions

}
