/**
 * $File: MV_MusicVisualizer.cs $
 * $Date: 2018-09-24 23:01:07 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information 
 *	                 Copyright © 2018 by Shen, Jen-Chieh $
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JCSUnity;


/// <summary>
/// Play the music in different rule.
///   - loop
///   - random.
///   
/// SOURCE(jenchieh): https://www.youtube.com/watch?v=V_8JSWVT36k
/// </summary>
public class MV_MusicVisualizer
    : MonoBehaviour 
{

    /*******************************************/
    /*            Public Variables             */
    /*******************************************/

    /*******************************************/
    /*           Private Variables             */
    /*******************************************/

    [Header("** Check Variables (MV_MusicVisualizer) **")]

    [Tooltip("Cubes to do the scaling.")]
    [SerializeField]
    private List<MV_VisualizeObject> mVisualizeObjects = null;


    [Header("** Runtime Variables (MV_MusicVisualizer) **")]

    [Tooltip("")]
    [SerializeField]
    private MV_VisualizeObject mOriginClone = null;

    [Tooltip("How many visualize object to spawn?")]
    [SerializeField]
    private int mVOToSpawn = 50;

    [Tooltip("Times to spectrum data.")]
    [SerializeField]
    [Range(1.0f, 500.0f)]
    private float mProduction = 10.0f;

    [Tooltip("Minimum scale")]
    [SerializeField]
    [Range(0.0f, 5.0f)]
    private float mMinScale = 0.1f;

    [Tooltip("Radius of the cubes' revolution.")]
    [SerializeField]
    [Range(0.0f, 1000.0f)]
    private float mRadius = 20.0f;

    [Tooltip("Current degree.")]
    [SerializeField]
    private float mDegree = 0;

    [Tooltip("")]
    [SerializeField]
    private float mDegreePerSec = 0.1f;

    [Tooltip("")]
    [SerializeField]
    [Range(0.01f, 5.0f)]
    private float mFriction = 0.1f;

    /*******************************************/
    /*           Protected Variables           */
    /*******************************************/

    /*******************************************/
    /*             setter / getter             */
    /*******************************************/

    /*******************************************/
    /*            Unity's function             */
    /*******************************************/
    private void Awake()
    {
        // First spawn it.
        for (int index = 0;
            index < mVOToSpawn;
            ++index)
        {
            MV_VisualizeObject vo = JCS_Utility.SpawnGameObject(mOriginClone) as MV_VisualizeObject;

            mVisualizeObjects.Add(vo);

            vo.transform.position = this.transform.position;

            vo.transform.SetParent(this.transform);
        }
    }

    private void Start()
    {
        for (int index = 0;
            index < mVisualizeObjects.Count;
            ++index)
        {
            MV_VisualizeObject vo = mVisualizeObjects[index];

            vo.revoAct.Degree = (int)(360.0f / (float)mVisualizeObjects.Count * (float)index);

            vo.revoAct.Radius = mRadius;

            vo.revoAct.Origin = this.transform;
        }
    }

    private void Update()
    {
        float[] spectrum = new float[50];
        AudioListener.GetOutputData(spectrum, 0);

        mDegree += mDegreePerSec * Time.deltaTime;

        int len = mVisualizeObjects.Count;

        for (int index = 0;
            index < len;
            ++index)
        {
            MV_VisualizeObject vo = mVisualizeObjects[index];

            int specIndex = index + index;

            if (specIndex >= len)
                specIndex = specIndex - len + 1;

            vo.targetScale.y = spectrum[specIndex] * mProduction;

            if (vo.targetScale.y < mMinScale)
                vo.targetScale.y = mMinScale;

            vo.friction = mFriction;

            // This will make the degree go in one direction.
            {
                int degreeInterval = (int)(360.0f / (float)len * (float)index);

                vo.revoAct.Degree = degreeInterval + (int)mDegree;
            }
        }
    }

    /*******************************************/
    /*              Self-Define                */
    /*******************************************/
    //----------------------
    // Public Functions

    //----------------------
    // Protected Functions

    //----------------------
    // Private Functions

}
