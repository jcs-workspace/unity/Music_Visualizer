/**
 * $File: MV_ButtonText.cs $
 * $Date: 2018-09-25 17:46:06 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information 
 *	                 Copyright © 2018 by Shen, Jen-Chieh $
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using JCSUnity;


/// <summary>
/// Text for the 3d button.
/// </summary>
[RequireComponent(typeof(JCS_PositionCastAction))]
[RequireComponent(typeof(JCS_FadeObject))]
[RequireComponent(typeof(Text))]
public class MV_ButtonText 
    : MonoBehaviour 
{

    /*******************************************/
    /*            Public Variables             */
    /*******************************************/

    /*******************************************/
    /*           Private Variables             */
    /*******************************************/

    private JCS_PositionCastAction mPCA = null;

    private JCS_FadeObject mFadeObject = null;

    private Text mTextComp = null;

    /*******************************************/
    /*           Protected Variables           */
    /*******************************************/

    /*******************************************/
    /*             setter / getter             */
    /*******************************************/
    public JCS_PositionCastAction PCA { get { return this.mPCA; } }
    public JCS_FadeObject FadeObject { get { return this.mFadeObject; } }
    public Text TextComp { get { return this.mTextComp; } }

    /*******************************************/
    /*            Unity's function             */
    /*******************************************/
    private void Awake()
    {
        this.mPCA = this.GetComponent<JCS_PositionCastAction>();
        this.mFadeObject = this.GetComponent<JCS_FadeObject>();
        this.mTextComp = this.GetComponent<Text>();
    }
    
    /*******************************************/
    /*              Self-Define                */
    /*******************************************/
    //----------------------
    // Public Functions
    
    //----------------------
    // Protected Functions
    
    //----------------------
    // Private Functions
    
}
