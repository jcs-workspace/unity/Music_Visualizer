/**
 * $File: MV_VisualizeObject.cs $
 * $Date: 2018-09-25 01:38:47 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information 
 *	                 Copyright © 2018 by Shen, Jen-Chieh $
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using JCSUnity;


/// <summary>
/// Each visualize object.
/// </summary>
[RequireComponent(typeof(JCS_RevolutioAction))]
public class MV_VisualizeObject 
    : MonoBehaviour 
{

    /*******************************************/
    /*            Public Variables             */
    /*******************************************/

    public JCS_RevolutioAction revoAct = null;

    public Vector3 targetScale = Vector3.zero;

    public float friction = 0.1f;

    /*******************************************/
    /*           Private Variables             */
    /*******************************************/

    /*******************************************/
    /*           Protected Variables           */
    /*******************************************/

    /*******************************************/
    /*             setter / getter             */
    /*******************************************/

    /*******************************************/
    /*            Unity's function             */
    /*******************************************/
    private void Awake()
    {
        revoAct = this.GetComponent<JCS_RevolutioAction>();

        // set to the initial scale.
        targetScale = this.transform.localScale;
    }

    private void Update()
    {
        this.transform.localScale += (targetScale - this.transform.localScale) / friction * Time.deltaTime;
    }
    
    /*******************************************/
    /*              Self-Define                */
    /*******************************************/
    //----------------------
    // Public Functions
    
    //----------------------
    // Protected Functions
    
    //----------------------
    // Private Functions
    
}
