/**
 * $File: MV_GameManager.cs $
 * $Date: 2018-09-24 22:46:43 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information 
 *	                 Copyright © 2018 by Shen, Jen-Chieh $
 */
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using JCSUnity;


/// <summary>
/// MV's game manager.
/// </summary>
public class MV_GameManager 
    : JCS_Managers<MV_GameManager>
{

    /*******************************************/
    /*            Public Variables             */
    /*******************************************/

    [Tooltip("Directory to put all the music in.")]
    public string MUSIC_ROOT_PATH = "/mv_dir/";

    [SerializeField]
    public string MUSIC_EXTENSION = "*.ogg";

    /*******************************************/
    /*           Private Variables             */
    /*******************************************/

    /*******************************************/
    /*           Protected Variables           */
    /*******************************************/

    /*******************************************/
    /*             setter / getter             */
    /*******************************************/

    /*******************************************/
    /*            Unity's function             */
    /*******************************************/
    private void Awake()
    {
        instance = this;

        // Setup music path.
        MUSIC_ROOT_PATH = Application.dataPath + MUSIC_ROOT_PATH;

        if (!Directory.Exists(MUSIC_ROOT_PATH))
            Directory.CreateDirectory(MUSIC_ROOT_PATH);
    }

    /*******************************************/
    /*              Self-Define                */
    /*******************************************/
    //----------------------
    // Public Functions
    
    //----------------------
    // Protected Functions
    
    //----------------------
    // Private Functions
    
}
