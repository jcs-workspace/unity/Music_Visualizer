/**
 * $File: MV_UIManager.cs $
 * $Date: 2018-09-25 04:43:12 $
 * $Revision: $
 * $Creator: Jen-Chieh Shen $
 * $Notice: See LICENSE.txt for modification and distribution information 
 *	                 Copyright © 2018 by Shen, Jen-Chieh $
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using JCSUnity;


/// <summary>
/// UI manager.
/// </summary>
public class MV_UIManager 
    : JCS_Managers<MV_UIManager> 
{

    /*******************************************/
    /*            Public Variables             */
    /*******************************************/

    public Text TXT_SONG_NAME = null;

    public MV_PlayPauseButton BTN_PLAY_PAUSE = null;

    /*******************************************/
    /*           Private Variables             */
    /*******************************************/

    /*******************************************/
    /*           Protected Variables           */
    /*******************************************/

    /*******************************************/
    /*             setter / getter             */
    /*******************************************/

    /*******************************************/
    /*            Unity's function             */
    /*******************************************/
    private void Awake()
    {
        instance = this;
    }
    
    /*******************************************/
    /*              Self-Define                */
    /*******************************************/
    //----------------------
    // Public Functions
    
    //----------------------
    // Protected Functions
    
    //----------------------
    // Private Functions
    
}
