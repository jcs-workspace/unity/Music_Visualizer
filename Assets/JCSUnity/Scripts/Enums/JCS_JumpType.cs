﻿using UnityEngine;
using System.Collections;


namespace JCSUnity
{
    /// <summary>
    /// List of type of jumping action.
    /// </summary>
    public enum JCS_JumpeType
    {
        BASIC_JUMP = 1,
        DOUBLE_JUMP = 2,
        TRIPLE_JUMP = 3,
        DOUBLE_JUMP_FORCE = 4,
        TRIPLE_JUMP_FORCE = 5
    }
}
