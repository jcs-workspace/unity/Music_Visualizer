2018-10-01
* Update Unity Engine version to 2018.2.10f1. (Music_Visualizer)
* Update project version to 1.8.5. (Music_Visualizer)

2018-09-25
* First release. (Music_Visualizer)
* First setup project => Music_Visualizer. (Music_Visualizer)
